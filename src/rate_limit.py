from functools import wraps
import time
from typing import Dict

from fastapi import HTTPException

# 1 MB
MAX_SIZE = 1024 * 1024

# We are using in memory dictionary here, but if we want this to work properly
# with multiple workers we'll need to use external database or cache
requests_per_ip: Dict[str, Dict[float, int]] = {}


class MaxDownloadRateHandler(object):
    def __init__(self, ip: str) -> None:
        self.timestamp = time.time()
        self.ip = ip

    def is_limit_reached(self) -> bool:
        downloaded_bytes = self._get_latest_bytes_per_ip()
        return downloaded_bytes > MAX_SIZE

    def update_bytes_per_ip(self, bytes: int) -> None:
        requests_per_ip[self.ip] = {
            self.timestamp: bytes,
            **self._get_latest_requests_per_ip(),
        }

    def _get_latest_requests_per_ip(self) -> Dict:
        return {
            ts: bytes
            for ts, bytes in requests_per_ip.get(self.ip, {}).items()
            if (self.timestamp - ts) < 60
        }

    def _get_latest_bytes_per_ip(self) -> int:
        return sum(bytes for bytes in self._get_latest_requests_per_ip().values())


# This could be extended (together with the handler) to receive the time range and
# the max limit
def download_rate_limit(wrapped_function):
    """A decorator function that sets up a max download rate limit for one
    minute rolling window.

    :param wrapped_function: A view function that returns a FileResponse
                             with a 'Content-Length' header
    :type wrapped_function: function
    :return: The wrapped function
    :rtype: function
    """
    @wraps(wrapped_function)
    async def wrapper(*args, **kwargs):
        # If the request object isn't passed in the arguments we can't find the ip
        if 'request' not in kwargs:
            return await wrapped_function(*args, **kwargs)

        rate_handler = MaxDownloadRateHandler(kwargs['request'].client.host)
        if rate_handler.is_limit_reached():
            raise HTTPException(status_code=429, detail='Download limit reached')

        response = await wrapped_function(*args, **kwargs)

        rate_handler.update_bytes_per_ip(int(response.headers['Content-Length']))

        return response

    return wrapper
