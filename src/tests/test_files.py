import hashlib
import tempfile

from dataclasses import dataclass
from httpx import AsyncClient
from operator import itemgetter
from pytest_asyncio import fixture
from pytest import mark

import main
import rate_limit


@dataclass
class MockFileData():
    filename: str
    data: str

    @property
    def id(self):
        return hashlib.md5(self.filename.encode('utf-8')).hexdigest()

    def get_dict_value(self):
        return {
            'name': self.filename,
            'url': f'/tmp/{self.id}',
        }


async def send_file_data(client, file):
    with tempfile.NamedTemporaryFile(suffix=file.filename) as f:
        f.write(file.data.encode('utf-8'))
        # Return to the beginning of the file so when we send the file
        # the data is sent from the start
        f.seek(0)
        response = await client.post(
            '/files',
            files={
                'file': (file.filename, f, 'application/octet-stream'),
            },
        )
    return response


def verify_get_files_responses(returned_response, expected_data):
    assert returned_response.status_code == 200

    returned_data = returned_response.json()
    assert 'files' in returned_data

    sorted_returned_data = sorted(returned_data['files'], key=itemgetter('name'))
    sorted_expected_data = sorted(expected_data['files'], key=itemgetter('name'))
    assert sorted_returned_data == sorted_expected_data


@fixture
async def client():
    async with AsyncClient(app=main.app, base_url='http://test') as client:
        yield client


@fixture
async def initial_data(client):
    initial_data = [
        MockFileData('file_1', 'random data in file_1'),
        MockFileData('file_2', 'random data in file_2'),
        MockFileData('file_3', 'random data in file_4'),
    ]
    for f in initial_data:
        response = await send_file_data(client, f)

    return initial_data


@fixture(autouse=True)
async def database():
    yield None
    rate_limit.requests_per_ip = {}
    main.files_db = {}
    main.files_per_user_db = {}


@fixture
async def smaller_max_download_size():
    previous_size = rate_limit.MAX_SIZE
    rate_limit.MAX_SIZE = 5
    yield None
    rate_limit.MAX_SIZE = previous_size


@fixture
async def smaller_max_number_of_files():
    previous_size = main.MAX_FILES
    main.MAX_FILES = 1
    yield None
    main.MAX_FILES = previous_size


@mark.asyncio
async def test_insert_files_returns_200(client):
    file_data = MockFileData('file_1', 'random \x1231d data in file_1')
    response = await send_file_data(client, file_data)
    assert response.status_code == 200
    assert response.json() == file_data.get_dict_value()


@mark.asyncio
async def test_insert_files_returns_400_when_max_reached(client, smaller_max_number_of_files):
    file_data_1 = MockFileData('file_1', 'random \x1231d data in file_1')
    file_data_2 = MockFileData('file_2', 'random \x1232d data in file_2')

    response = await send_file_data(client, file_data_1)
    # First time the insert goes correctly
    assert response.status_code == 200
    assert response.json() == file_data_1.get_dict_value()

    # The second time it fails as we've already reached the max size
    response = await send_file_data(client, file_data_2)
    assert response.status_code == 200


@mark.asyncio
async def test_get_files_returns_200_empty(client):
    response = await client.get('/files')
    assert response.status_code == 200
    assert response.json() == {'files': []}


@mark.asyncio
async def test_get_files_returns_200_with_files(client, initial_data):
    expected_data = {'files': [d.get_dict_value() for d in initial_data]}
    response = await client.get('/files')
    verify_get_files_responses(response, expected_data)


@mark.asyncio
async def test_download_file_returns_200_and_file(client):
    initial_data = MockFileData('file_1', 'random data in file_1')
    await send_file_data(client, initial_data)

    response = await client.get(f'/files/{initial_data.id}')
    assert response.status_code == 200
    assert response.content == initial_data.data.encode('utf-8')


@mark.asyncio
async def test_download_file_returns_429_when_size_too_big(client, smaller_max_download_size):
    initial_data = MockFileData('file_1', 'random data in file_1')
    await send_file_data(client, initial_data)

    # First time it works as the check is done after
    response = await client.get(f'/files/{initial_data.id}')
    assert response.status_code == 200
    assert response.content == initial_data.data.encode('utf-8')

    # Second time it fails
    response = await client.get(f'/files/{initial_data.id}')
    assert response.status_code == 429
