import hashlib
import os
import shutil

from dataclasses import dataclass
from typing import Dict, Set, Tuple

from fastapi import FastAPI, HTTPException, Request, UploadFile
from fastapi.responses import FileResponse

from rate_limit import download_rate_limit

app = FastAPI()

USER_ID = 1
MAX_FILES = 99

BASE_PATH = '/tmp/'


@dataclass(init=False)
class FileDTO():
    """This is our "model" in the local database.

    It contains the basic information that we need to store per file
    """
    name: str
    id: str
    size: int

    def __init__(self, file: UploadFile) -> None:
        self.name = file.filename
        self.id = hashlib.md5(file.filename.encode('utf-8')).hexdigest()
        self.size = self.__store_file_on_disk(file)

    @property
    def url(self):
        return os.path.join(BASE_PATH, self.id)

    def __store_file_on_disk(self, file: UploadFile) -> int:
        with open(self.url, 'wb') as new_f:
            shutil.copyfileobj(file.file, new_f)
            # Tell would bhe set at the last byte as we just copied
            # everything from the temp file
            file_size = new_f.tell()

        return file_size

    def get_file_response(self) -> FileResponse:
        return FileResponse(
            path=self.url,
            filename=self.name,
            media_type='application/octet-stream',
            headers={
                'Content-Length': str(self.size),
            },
        )

    def get_dict_response(self) -> Dict:
        return {
            'name': self.name,
            'url': self.url,
        }


files_db: Dict[str, FileDTO] = {}
files_per_user_db: Dict[int, Set[str]] = {}


@app.get('/health')
async def root():
    return {}


@app.post('/files')
async def upload_file(file: UploadFile):
    if len(files_db) > MAX_FILES:
        raise HTTPException(status_code=400, detail='File has been rejected')

    db_file = FileDTO(file)
    files_db[db_file.id] = db_file
    files_per_user_db.setdefault(USER_ID, set()).add(db_file.id)
    return db_file.get_dict_response()


@app.get('/files')
async def get_files():
    return {
        'files': [
            files_db[file_id].get_dict_response()
            for file_id in files_per_user_db.get(USER_ID, [])
        ]
    }


@app.get('/files/{file_id}')
@download_rate_limit
async def get_file(file_id: str, request: Request):
    if file_id not in files_db:
        raise HTTPException(status_code=404, detail='File not found')

    file = files_db[file_id]
    return file.get_file_response()
